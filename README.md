# Autenticación con servicios de Google

Google determina la identidad del usuario mediante una cuenta de Google que normalmente está asociada al proyecto esto lo usa para autenticar al usuario, cabe recalcar la diferencia que existe entre autenticación que se refiere a identificar quien es usted, mientras que la autorización se refiere a lo que puede hacer.


## Requisitos
* Crear clave de cuenta de servicio

 ![](imagenes/clave.png)

 Al crear una clave se descargará un JSON. 

 ## Configuración de variables de entorno

* Es recomendable almacenar el JSON de sus credenciales dentro de una carpeta en su proyecto. 

* Configurar estas variables de entorno permite autenticar la sesión en la que corre su app. Es por esto que deberá correr estos comandos en la terminal cada vez que abra una nueva sesión. 

**Para LINUX O MACOS**
```
exportar GOOGLE_APPLICATION_CREDENTIALS = "[PATH]"
```
Por ejemplo:
```
exportar GOOGLE_APPLICATION_CREDENTIALS = "/home/user/Downloads/[FILE_NAME.json"
```

**Para WINDOWS**
````
$ env : GOOGLE_APPLICATION_CREDENTIALS = "[PATH]"
````
Por ejemplo:

```
$ env : GOOGLE_APPLICATION_CREDENTIALS = "C: \ Users \ username \ Downloads \ [FILE_NAME] .json"
```

## Verificar autenticación

Una vez que se configuro la variable de entorno en la terminal, si desea puede verificar con el siguiente controller.

``` TypeScript
@Controller()

export class AuthController {

    projectId = 'Tu projectID en google cloud'
    bucketName = 'nombre de su bucket '

    
    start(projectId,bucketName)
    {
        const storage = new Storage({projectId});
        storage.createBucket(bucketName);
        console.log(`Bucket ${bucketName} created.`)
    }       
}

```
<a href="https://twitter.com/alimonse29" target="_blank"><img alt="Sígueme en Twitter" height="35" width="35" src="https://3.bp.blogspot.com/-E4jytrbmLbY/XCrI2Xd_hUI/AAAAAAAAIAo/qXt-bJg1UpMZmTjCJymxWEOGXWEQ2mv3ACLcBGAs/s1600/twitter.png" title="Sígueme en Twitter"/> @alimonse29 </a><br>

